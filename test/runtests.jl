
using Test
import SomeProject

SP = SomeProject

p = SP.Plan(Vector{SP.PlannedProcess}())
(a,b) = SP.pop_processes(p, 0)
@test length(a) == 0
@test length(b) == 0


p = SP.Plan([SP.CreateProcess(1, Set{SP.Group}()), 
             SP.CreateProcess(2, Set{SP.Group}())])
(a,b) = SP.pop_processes(p, 0)
@test length(a) == 0
@test length(b) == 2

(a,b) = SP.pop_processes(p, 1)
@test length(a) == 1
@test length(b) == 1

(a,b) = SP.pop_processes(p, 2)
@test length(a) == 1
@test length(b) == 0

(a,b) = SP.pop_processes(p, 3)
@test length(a) == 0
@test length(b) == 0


s = SP.State(1, Dict{SP.Contract, SP.Group}(), SP.Organization())
c_g = SP.apply_processes(s, Vector{SP.PlannedProcess}())
@test length(c_g) == 0

gA = SP.Group(Dict(:x => :A))
gB = SP.Group(Dict(:x => :B))
gC = SP.Group(Dict(:x => :C))
T = Vector{SP.PlannedProcess}

c_g = SP.apply_processes(s, T([SP.CreateProcess(1, Set([gA, gB]))]))
@test Set(values(c_g)) == Set([gA, gB])


o = SP.Organization()
c1 = SP.Contract(SP.Resource(), o)
c2 = SP.Contract(SP.Resource(), o)
s = SP.State(1, Dict(c1 => gA, c2 => gB), o)

c_g = SP.apply_processes(s, T([SP.TerminateProcess(1, Set{SP.Contract}())]))
@test Set(values(c_g)) == Set([gA, gB])

c_g = SP.apply_processes(s, T([SP.TerminateProcess(1, Set([c1]))]))
@test Set(values(c_g)) == Set([gB])

c_g = SP.apply_processes(s, T([SP.TerminateProcess(1, Set([c1, c2]))]))
@test length(Set(values(c_g))) == 0



c_g = SP.apply_processes(s, T([SP.MoveProcess(1, Dict{SP.Contract, SP.Group}())]))
@test Set(values(c_g)) == Set([gA, gB])

c_g = SP.apply_processes(s, T([SP.MoveProcess(1, Dict(c1 => gC))]))
@test Set(values(c_g)) == Set([gB, gC])

c_g = SP.apply_processes(s, T([SP.MoveProcess(1, Dict(c1 => gC, c2 => gA))]))
@test Set(values(c_g)) == Set([gA, gC])



