
function get_process_effects(p :: MoveProcess)
end

function get_process_effects(p :: TerminateProcess)
end

function get_process_effects(p :: CreateProcess)
end



function pop_processes(plan::Plan, time::Time)
    from_ix = findfirst(x -> x.time >= time, plan.time_ordered_processes)
    if isnothing(from_ix)
        return (Vector{PlannedProcess}(), Vector{PlannedProcess}())
    end
    
    upper_ix = findfirst(x -> x.time > time, plan.time_ordered_processes)
    to_ix = isnothing(upper_ix) ? length(plan.time_ordered_processes) : upper_ix - 1
    
    return (plan.time_ordered_processes[from_ix:to_ix], plan.time_ordered_processes[(to_ix + 1):end])
end


function apply_processes(state::State, processes::Vector{PlannedProcess})
    contract_group = copy(state.contract_group)
    for p in processes
        if isa(p, MoveProcess)
            for (c,g) in p.contract_target
                contract_group[c] = g
            end
        elseif isa(p, TerminateProcess)
            for c in p.contract
                delete!(contract_group, c)
            end
        elseif isa(p, CreateProcess)
            for g in p.target
                c = Contract(Resource(), state.organization)
                contract_group[c] = g
            end
        end
    end
    contract_group
end

function execute_time_step(state::State, plan::Plan)
    new_time = state.time + 1
    processes, future_processes = pop_processes(plan, new_time)
    new_contract_group = apply_processes(state, processes)
    new_state = State(new_time, new_contract_group, state.organization)
    new_plan = Plan(future_processes)
    (new_state, new_plan)
end

function simulate_time_step()
    # stochastic effects of attrition
end

