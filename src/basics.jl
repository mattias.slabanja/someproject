import UUIDs


Time = Int64


struct Resource 
    id :: UUIDs.UUID
    Resource() = new(UUIDs.uuid4()) 
end


mutable struct Organization
    _next_contract_number :: Threads.Atomic{Int64}
    Organization() = new(Threads.Atomic{Int64}(1))
end

function get_next_contract_ix!(o :: Organization)
    Threads.atomic_add!(o._next_contract_number, 1)
end


struct Contract
    resource :: Resource
    contract_number :: Int64
    Contract(r :: Resource, o :: Organization) = new(r, get_next_contract_ix!(o))
end


struct Group 
    properties :: Dict{Symbol, Symbol}
end


struct ProcessEffect
    time :: Time
    effect :: Dict{Group, Int64}
    consumption :: Dict{Group, Float64}
end


abstract type PlannedProcess end

function get_process_effect(p :: PlannedProcess)
    error("not implemented")
end


struct MoveProcess <: PlannedProcess
    time :: Time
    contract_target :: Dict{Contract, Group}
end

struct TerminateProcess <: PlannedProcess
    time :: Time
    contract :: Set{Contract}
end

struct CreateProcess <: PlannedProcess
    time :: Time
    target :: Set{Group}
end


struct State
    time :: Time
    contract_group :: Dict{Contract, Group}
    organization :: Organization
end


struct Plan
    time_ordered_processes :: Vector{PlannedProcess}
end




