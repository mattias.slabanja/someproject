
if isfile("Project.toml") && isfile("Manifest.toml")
    import Pkg
    Pkg.activate(".")

    println("Using Revise")
    using Revise
end
